<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;

    protected $fillable = ['tanggal_transaksi', 'jumlah_terjual','barang_id'];

    public function barang(){
        return $this->belongsTo(Barang::class);
    }
}
