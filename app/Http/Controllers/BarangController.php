<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->nama != NULL) {
            $barang = Barang::where('nama', 'like',"%".$request->nama."%")
            ->paginate(10);
        } else {
            $barang = Barang::paginate(10);
        }
        
        return view('content.barang', compact('barang'));
    }

    public function transaksi(Request $request)
    {
        $from = $request->from;
        $until = $request->until;
        $barang = Barang::join('transaksis', 'transaksis.barang_id', '=', 'barangs.id')
                ->groupBy('transaksis.barang_id')
                ->selectRaw('barangs.nama, barangs.stok , sum(jumlah_terjual) as sum')
                ->withCount('transaksi')
                ->whereBetween('tanggal_transaksi', [$from, $until])
                ->orderBy('sum', 'desc')
                ->paginate(20);
        
        
        return view('content.transaksiBarang', compact('barang', 'from', 'until'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $barang = new Barang;
        $barang->nama = $request->nama;
        $barang->stok = $request->stok;
        $barang->jenis_barang = $request->jenis_barang;
        $barang->save();
        return redirect('/barang')->with('message', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $barang = Barang::find($id);
        return view('content.showBarang', compact('barang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $barang = Barang::find($id);
        $jenis = [
            '1' => 'konsumsi',
            '2' => 'pembersih'
        ];
        return view('content.editBarang', compact('barang', 'jenis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $barang = Barang::find($id);
        $barang->nama = $request->nama;
        $barang->stok = $request->stok;
        $barang->jenis_barang = $request->jenis_barang;
        $barang->save();
        return redirect('/barang')->with('message', 'Data berhasil ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Barang::destroy($id);
        return redirect('/barang')->with('message', 'Data berhasil dihapus');
    }
}
