<?php

namespace App\Http\Controllers;

use App\DataTables\TransaksiDataTable;
use App\Models\Barang;
use App\Models\Transaksi;
use Illuminate\Http\Request;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->tanggal_transaksi != NULL) {
            $transaksi = Transaksi::with('barang')
                        ->whereDate('tanggal_transaksi', '=', $request->tanggal_transaksi)
                        ->paginate(20);
        } else {
            $transaksi = Transaksi::with('barang')->paginate(20);
        }
            $barang = Barang::all();
            $sort = [
                'dateDesc' => 'Tanggal Transaksi',
                'nameDesc' => 'Nama Barang'
            ];
            return view('content.dashboard', compact('transaksi','barang', 'sort'));   
    }

    public function sorting($id){

        if ($id == "dateDesc") {
            $transaksi = Transaksi::with('barang')
                        ->orderBy('tanggal_transaksi', 'desc')
                        ->paginate(20);
            $sort = [
                'dateAsc' => 'Tanggal Transaksi ',
                'nameDesc' => 'Nama Barang'
            ];       
        } else if($id == "dateAsc") {
            $transaksi = Transaksi::with('barang')
                        ->orderBy('tanggal_transaksi', 'asc')
                        ->paginate(20);
            $sort = [
                'dateDesc' => 'Tanggal Transaksi',
                'nameDesc' => 'Nama Barang'
            ];
        } elseif($id == "nameDesc") {
            $transaksi = Transaksi::with('barang')
                        ->join('barangs', 'transaksis.barang_id', '=', 'barangs.id')
                        ->orderBy('barangs.nama', 'desc')
                        ->paginate(20);
            $sort = [
                'dateDesc' => 'Tanggal Transaksi',
                'nameAsc' => 'Nama Barang'
            ];  
        } elseif($id == "nameAsc") {
            $transaksi = Transaksi::with('barang')
                        ->join('barangs', 'transaksis.barang_id', '=', 'barangs.id')
                        ->orderBy('barangs.nama', 'asc')
                        ->paginate(20);
            $sort = [
                'dateDesc' => 'Tanggal Transaksi',
                'nameAsc' => 'Nama Barang'
            ];  
        }
        
        $barang = Barang::all();
        return view('content.dashboard', compact('transaksi','barang', 'sort'));
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $barang = Barang::find($request->barang_id);
        if ($barang->stok - $request->jumlah_terjual >= 0) {
            $transaksi = new Transaksi;
            $transaksi->tanggal_transaksi = $request->tanggal_transaksi;
            $transaksi->jumlah_terjual = $request->jumlah_terjual;
            $transaksi->barang_id = $request->barang_id;
            $barang->stok = $barang->stok - $request->jumlah_terjual;
            $barang->save();
            $transaksi->save();
            return redirect('/')->with('message', 'Data berhasil ditambahkan');
        }else {
            return redirect('/')->with('message', 'Jumlah barang tidak cukup. silahkan tambahkan stok barang terlebih dahulu ');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Transaksi::find($id);
        $barang = Barang::All();
        return view('content.edit', compact('data','barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaksi = Transaksi::find($id);
        $transaksi->tanggal_transaksi = $request->tanggal_transaksi;
        $transaksi->jumlah_terjual = $request->jumlah_terjual;
        $transaksi->barang_id = $request->barang_id;
        $transaksi->save();
        return redirect('/')->with('message', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaksi = Transaksi::destroy($id);
        return redirect('/')->with('message','Data berhasil dihapus!');
    }
}
