<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\BarangController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//transaksi
Route::get('/', [AdminController::class, 'index']);
Route::post('/', [AdminController::class, 'store']);
Route::get('/transaksi/{transaksi}/edit', [AdminController::class, 'edit']);
Route::delete('/transaksi/{transaksi}', [AdminController::class, 'destroy']);
Route::patch('/transaksi/{transaksi}', [AdminController::class, 'update']);
Route::get('/sort/{transaksi}', [AdminController::class, 'sorting']);

//barang
Route::get('/barang', [BarangController::class, 'index']);
Route::post('/barang', [BarangController::class, 'store']);
Route::get('/barang/{barang}/edit', [BarangController::class, 'edit']);
Route::delete('/barang/{barang}', [BarangController::class, 'destroy']);
Route::patch('/barang/{barang}', [BarangController::class, 'update']);
Route::get('/barang/{barang}', [BarangController::class, 'show']);
Route::get('/transaksi/barang', [BarangController::class, 'transaksi']);