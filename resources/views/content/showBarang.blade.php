@extends('template.master')
@section('title')
    Warehouse 
@endsection
@section('subTitle')
    Data Barang di toko XYZ
@endsection
@section('content')
<div class="row">
  
</div>
<div class="row">
  <div class="col-md-4">
    <div class="card" >
      <div class="card-body">
        <h5 class="card-title">{{$barang->nama}}</h5>
        <h6 class="card-title mb-2">Stok : {{$barang->stok}}</h6>
        <p class="card-text">Jenis Barang : {{$barang->jenis_barang}}</p>
        <a href="/barang" class="card-link">Tampilkan semua barang</a>
        <a href="/" class="card-link">Kembali</a>
      </div>
    </div>
  </div>
</div>


    
@endsection