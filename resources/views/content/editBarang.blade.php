@extends('template.master')
@section('title')
    Edit Data Penjualan
@endsection
@section('subTitle')
    Silahkan Masukkan data yang ingin di edit
@endsection
@section('content')

<div class="row">
  <div class="col-md-6">
    <form action="/barang/{{$barang->id}}" method="POST">
      @csrf
      <input type="hidden" name="_method" value="patch">
      <div class="mb-3">
        <label for="nama" class="form-label">Nama Barang</label>
        <input type="text" class="form-control" name="nama" id="nama" value="{{$barang->nama}}" required>
      </div>
      
      <div class="mb-3">
        <label for="stok" class="form-label">Stok</label>
        <input type="number" class="form-control" id="stok" name="stok" value="{{$barang->stok}}" required>
      </div>

      <div class="mb-3">
        <label for="jenis_barang" class="form-label">Jenis Barang</label>
        <select name="jenis_barang" id="jenis_barang" class="form-control" required>
          <option value="">Pilih Jenis Barang</option>
          @foreach ($jenis as $id => $item)
          <option value="{{$id}}"
          @if ($item == $barang->jenis_barang)
              selected
          @endif
          >{{$item}}</option>
          @endforeach
          
        </select>
      </div>
  </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary">Save changes</button>
  </div>
</form>
  </table>
  </div>
</div>


    
@endsection