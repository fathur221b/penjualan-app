@extends('template.master')
@section('title')
    Dashboard 
@endsection
@section('subTitle')
    Data Penjualan Toko XYZ tahun 2021
@endsection
@section('content')
<div class="row justify-content-md-center border-bottom mb-3 pb-3" >
  <div class="col-auto">
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      Tambah Transaksi Baru
    </button>
  </div>
  <div class="col-auto">
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#transaksiBarang">
      Banyak Transaksi per Barang
    </button>
  </div>
  <div class="col-auto">
    <form action="/" method="GET">
      <div class="input-group">
          <input name="tanggal_transaksi" class="form-control" type="date" placeholder="Cari transaksi" aria-label="Search for..." aria-describedby="btnNavbarSearch" />
          <button class="btn btn-primary" id="btnNavbarSearch" type="submit"><i class="fas fa-search"></i></button>
      </div>
  </form>
  </div>
  
</div>
<div class="row">
  <div class="col-md-12">
    <table class="table table-hover">
      <thead>
          <tr>
            <th scope="col">#</th>
            @foreach ($sort as $key => $value)
            <th scope="col"><a href="/sort/{{$key}}">{{$value}}  </a> </th>
            @endforeach
            
            <th scope="col">Jumlah Terjual</th>
            <th scope="col">Jenis Barang</th>
            <th scope="col">Action</th>
            
          </tr>
        </thead>
        <tbody>
            @foreach ($transaksi as $item)
            <tr>
                <th scope="row">{{ $loop->iteration }}</th>
                <td>{{$item->tanggal_transaksi}}</td>
                <td> <a href="/barang/{{$item->barang_id}}">{{$item->barang->nama}}</a> </td>
                <td>{{$item->jumlah_terjual}}</td>
                <td>{{$item->barang->jenis_barang}}</td>
                <td><a class="badge bg-success" href="transaksi/{{$item->id}}/edit">Edit</a>
                  <form action="/transaksi/{{$item->id}}" method="POST" class="d-inline">
                    @csrf 
                    <input type="hidden" name="_method" value="delete">
                    <button type="submit" class="badge bg-danger">Hapus</button></form> 
                  
                    
                  </td>
              </tr>
            @endforeach
        </tbody>
  </table>
  
  </div>
</div>
<div class="row">
  <div class="col-12 mt-3">
    {{ $transaksi->links() }}
  </div>
  
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Transaksi baru</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        {{-- form --}}
        <form action="/" method="POST">
          @csrf
          <div class="mb-3">
            <label for="transaksi" class="form-label">Tanggal Transaksi</label>
            <input type="date" class="form-control" name="tanggal_transaksi" id="transaksi" required>
          </div>
          <div class="mb-3">
            <label for="barang" class="form-label">Pilih barang</label>
            <select name="barang_id" id="barang" class="form-control" required>
              <option value="">Pilih barang</option>
              @foreach ($barang as $item)
              <option value="{{$item->id}}">{{$item->nama}}</option>
              @endforeach
            </select>
          </div>
          <div class="mb-3">
            <label for="jumlah" class="form-label">Jumlah Terjual</label>
            <input type="number" class="form-control" id="jumlah" name="jumlah_terjual" required>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="transaksiBarang" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Jumlah Transaksi per barang</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        {{-- form --}}
        <form action="/transaksi/barang" method="GET">
          @csrf
          <div class="mb-3">
            <label for="from" class="form-label">Dari Tanggal</label>
            <input type="date" class="form-control" name="from" id="from" required>
          </div>
          <div class="mb-3">
            <label for="until" class="form-label">Hingga Tanggal</label>
            <input type="date" class="form-control" name="until" id="until" required>
          </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
    </div>
  </div>
</div>
    
@endsection