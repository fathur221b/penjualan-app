@extends('template.master')
@section('title')
    Warehouse 
@endsection
@section('subTitle')
    Data Barang di toko XYZ
@endsection
@section('content')

<div class="row justify-content-md-center border-bottom mb-3 pb-3" >
  <div class="col-auto">
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      Tambah Data Baru
    </button>
  </div>
  
  <div class="col-auto">
    <form action="/barang" method="GET">
      <div class="input-group">
          <input name="nama" class="form-control" type="text" placeholder="Cari barang..." aria-label="Search for..." aria-describedby="btnNavbarSearch" />
          <button class="btn btn-primary" id="btnNavbarSearch" type="submit"><i class="fas fa-search"></i></button>
      </div>
  </form>
  </div>
  
</div>
<div class="row">
  <div class="col-md-12">
    <table class="table table-hover">
      <thead>
          <tr>
            <th scope="col">#</th>            
            <th scope="col">nama barang</th>
            <th scope="col">Stok</th>
            <th scope="col">Jenis Barang</th>
            <th scope="col">Action</th>
            
          </tr>
        </thead>
        <tbody>
            @foreach ($barang as $item)
            <tr>
                <th scope="row">{{ $loop->iteration }}</th>
                <td>{{$item->nama}}</td>
                <td>{{$item->stok}}</td>
                <td>{{$item->jenis_barang}}</td>
                <td><a class="badge bg-success" href="barang/{{$item->id}}/edit">Edit</a>
                  <form action="/barang/{{$item->id}}" method="POST" class="d-inline">
                    @csrf 
                    <input type="hidden" name="_method" value="delete">
                    <button type="submit" class="badge bg-danger">Hapus</button></form> 
                  
                    
                  </td>
              </tr>
            @endforeach
        </tbody>
  </table>
  </div>
</div>
<div class="row">
  <div class="col-12 mt-3">
    {{ $barang->links() }}
  </div>
  
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data baru</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        {{-- form --}}
        <form action="/barang" method="POST">
          @csrf
          <div class="mb-3">
            <label for="nama" class="form-label">Nama Barang</label>
            <input type="text" class="form-control" name="nama" id="nama" required>
          </div>
          
          <div class="mb-3">
            <label for="stok" class="form-label">Stok</label>
            <input type="number" class="form-control" id="stok" name="stok" required>
          </div>

          <div class="mb-3">
            <label for="jenis_barang" class="form-label">Jenis Barang</label>
            <select name="jenis_barang" id="jenis_barang" class="form-control" required>
              <option value="">Pilih Jenis Barang</option>
              <option value="1">Konsumsi</option>
              <option value="2">Pembersih</option>
            </select>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
    </div>
  </div>
</div>
    
@endsection