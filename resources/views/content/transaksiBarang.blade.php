@extends('template.master')
@section('title')
    Penjualan 
@endsection
@section('subTitle')
    Penjualan Barang di toko XYZ antara tanggal <b>{{date('d-m-Y', strtotime($from))}}</b> hingga <b>{{date('d-m-Y', strtotime($until))}}</b> 
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <table class="table table-hover">
      <thead>
          <tr>
            <th scope="col">#</th>            
            <th scope="col">nama barang</th>
            <th scope="col">Stok</th>
            <th scope="col">Jumlah Transaksi</th>
            <th scope="col">Jumlah Terjual</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($barang as $item)
            <tr>
                <th scope="row">{{ $loop->iteration }}</th>
                <td>{{$item->nama}}</td>
                <td>{{$item->stok}}</td>
                <td>{{$item->transaksi_count}}</td>
                <td>{{$item->sum}}</td>
                
                
                
              </tr>
            @endforeach
        </tbody>
  </table>
  </div>
</div>
<div class="row">
  <div class="col-12 mt-3">
    {{ $barang->links() }}
  </div>
  
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data baru</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        {{-- form --}}
        <form action="/barang" method="POST">
          @csrf
          <div class="mb-3">
            <label for="nama" class="form-label">Nama Barang</label>
            <input type="text" class="form-control" name="nama" id="nama" required>
          </div>
          
          <div class="mb-3">
            <label for="stok" class="form-label">Stok</label>
            <input type="number" class="form-control" id="stok" name="stok" required>
          </div>

          <div class="mb-3">
            <label for="jenis_barang" class="form-label">Jenis Barang</label>
            <select name="jenis_barang" id="jenis_barang" class="form-control" required>
              <option value="">Pilih Jenis Barang</option>
              <option value="1">Konsumsi</option>
              <option value="2">Pembersih</option>
            </select>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
    </div>
  </div>
</div>
    
@endsection