@extends('template.master')
@section('title')
    Edit Data Penjualan
@endsection
@section('subTitle')
    Silahkan Masukkan data yang ingin di edit
@endsection
@section('content')

<div class="row">
  <div class="col-md-6">
    <form action="/transaksi/{{$data->id}}" method="POST">
      @csrf
      <input type="hidden" name="_method" value="patch">
      <div class="mb-3">
        <label for="transaksi" class="form-label">Tanggal Transaksi</label>
        <input type="date" class="form-control" name="tanggal_transaksi" id="transaksi" value="{{$data->tanggal_transaksi}}">
      </div>
      <div class="mb-3">
        <label for="barang" class="form-label">Pilih barang</label>
        <select name="barang_id" id="barang" class="form-control" >
          <option value="">Pilih barang</option>
          @foreach ($barang as $item)
            @if ($item->id == $data->barang_id)
              <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
              <option value="{{$item->id}}">{{$item->nama}}</option>
            @endif
          @endforeach
        </select>
      </div>
      <div class="mb-3">
        <label for="jumlah" class="form-label">Jumlah Terjual</label>
        <input type="number" class="form-control" id="jumlah" name="jumlah_terjual" value="{{$data->jumlah_terjual}}">
      </div>
  </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary">Save changes</button>
  </div>
</form>
  </table>
  </div>
</div>


    
@endsection